package com.verticalbargraphiclibrary;

import android.graphics.Typeface;

/**
 * Created by josue on 8/9/16.
 */
public class ChartColumnItem {

        private int mColumnBackgroundColor;
        private int mBottomTextColor;
        private int mCenterTextColor;
        private float mCenterTextSize;
        private float mBottomTextSize;
        private Typeface mColumnCenterTextTypeface;
        private Typeface mColumnBottomTextTypeface;
        private String mBottomText;
        private String mCenterText;
        private double mRealValue;
        private float mColumnWidth;

        public ChartColumnItem(int mColumnBackgroundColor, int mBottomTextColor, int mCenterTextColor,
                               float mCenterTextSize, float mBottomTextSize, Typeface mColumnCenterTextTypeface,
                               Typeface mColumnBottomTextTypeface, String mBottomText,
                               String mCenterText, double mRealValue , float pColumnWidth) {

            this.mColumnBackgroundColor = mColumnBackgroundColor;
            this.mBottomTextColor = mBottomTextColor;
            this.mCenterTextColor = mCenterTextColor;
            this.mCenterTextSize = mCenterTextSize;
            this.mBottomTextSize = mBottomTextSize;
            this.mColumnCenterTextTypeface = mColumnCenterTextTypeface;
            this.mColumnBottomTextTypeface = mColumnBottomTextTypeface;
            this.mBottomText = mBottomText;
            this.mCenterText = mCenterText;
            this.mRealValue = mRealValue;
            this.mColumnWidth = pColumnWidth;
        }

        public float getColumnWidth() {
            return mColumnWidth;
        }

        public void setColumnWidth(float mColumnWidth) {
            this.mColumnWidth = mColumnWidth;
        }

        public float getCenterTextSize() {
            return mCenterTextSize;
        }

        public void setCenterTextSize(int mCenterTextSize) {
            this.mCenterTextSize = mCenterTextSize;
        }

        public float getBottomTextSize() {
            return mBottomTextSize;
        }

        public void setBottomTextSize(int mBottomTextSize) {
            this.mBottomTextSize = mBottomTextSize;
        }

        public int getColumnBackgroundColor() {
            return mColumnBackgroundColor;
        }

        public void setColumnBackgroundColor(int mColumnBackgroundColor) {
            this.mColumnBackgroundColor = mColumnBackgroundColor;
        }

        public int getBottomTextColor() {
            return mBottomTextColor;
        }

        public void setBottomTextColor(int mBottomTextColor) {
            this.mBottomTextColor = mBottomTextColor;
        }

        public int getCenterTextColor() {
            return mCenterTextColor;
        }

        public void setCenterTextColor(int mCenterTextColor) {
            this.mCenterTextColor = mCenterTextColor;
        }

        public Typeface getColumnCenterTextTypeface() {
            return mColumnCenterTextTypeface;
        }

        public void setColumnCenterTextTypeface(Typeface mColumnCenterTextTypeface) {
            this.mColumnCenterTextTypeface = mColumnCenterTextTypeface;
        }

        public Typeface getColumnBottomTextTypeface() {
            return mColumnBottomTextTypeface;
        }

        public void setColumnBottomTextTypeface(Typeface mColumnBottomTextTypeface) {
            this.mColumnBottomTextTypeface = mColumnBottomTextTypeface;
        }

        public String getBottomText() {
            return mBottomText;
        }

        public void setBottomText(String mBottomText) {
            this.mBottomText = mBottomText;
        }

        public String getCenterText() {
            return mCenterText;
        }

        public void setCenterText(String mCenterText) {
            this.mCenterText = mCenterText;
        }

        public double getRealValue() {
            return mRealValue;
        }

        public void setRealValue(double mRealValue) {
            this.mRealValue = mRealValue;
        }
    }
