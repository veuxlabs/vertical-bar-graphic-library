package com.verticalbargraphiclibrary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by daniel on 09/10/15.
 *
 */
public class HorizontalBarGraphicView extends View{

    private final int BAR_SIDE_MARGIN;
    private final int BAR_DEFAULT_AMOUNT = 2;
    private final int DEFAULT_TEXT_COLOR = Color.parseColor("#3d3d3d");
    private final int DEFAULT_BACKGROUND_COLOR = Color.parseColor("#f2f3f2");
    private final int DEFAULT_FOREGROUND_COLOR = Color.parseColor("#c41230");
    private ArrayList<ChartColumnItem> mItems;
    private ArrayList<Double> percentList;
    private ArrayList<Double> targetPercentList;
    private Paint textPaintSideValues;
    private Paint textPaintTopValues;
    private Paint bgPaint;
    private Paint fgPaint;
    private Rect rect;
    Context context;
    private float mTextSideHeight;
    private float mSideBarLabelXAxisPosition;
    double myMax = 0;
    private int topMargin;
    private boolean isTimeValues;

    private Runnable animator = new Runnable() {
        @Override
        public void run() {
            boolean needNewFrame = false;
            for (int i = 0; i < targetPercentList.size(); i++) {
                if (percentList.get(i) < targetPercentList.get(i)) {
                    percentList.set(i, percentList.get(i) + 0.02f);
                    needNewFrame = true;
                } else if (percentList.get(i) > targetPercentList.get(i)){
                    percentList.set(i, percentList.get(i) - 0.02f);
                    needNewFrame = true;
                }
                if(Math.abs(targetPercentList.get(i) - percentList.get(i)) < 0.02f){
                    percentList.set(i, targetPercentList.get(i));
                }
            }
            if (needNewFrame) {
                postDelayed(this, 1);
            }
            invalidate();
        }
    };

    public HorizontalBarGraphicView(Context context){
        this(context,null);
    }

    public HorizontalBarGraphicView(Context context, AttributeSet attrs){
        super(context, attrs);
        this.context = context;
        mItems = new ArrayList<>();
        bgPaint = new Paint();
        bgPaint.setAntiAlias(true);
        bgPaint.setColor(DEFAULT_BACKGROUND_COLOR);
        fgPaint = new Paint(bgPaint);
        fgPaint.setColor(DEFAULT_FOREGROUND_COLOR);
        rect = new Rect();
        topMargin = GraphicUtils.dip2px(context, 0);
        int textSize = GraphicUtils.sp2px(context, 15);
        mTextSideHeight = getResources().getDimension(R.dimen.last_week_horizontal_graphic_side_value_height);
        mSideBarLabelXAxisPosition = getResources().getDimension(R.dimen.last_week_horizontal_graphic_side_value_x_axis_position);
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.home_graphic_width, outValue, true);
        BAR_SIDE_MARGIN  = GraphicUtils.dip2px(context, 13);
        textPaintSideValues = new Paint();
        textPaintSideValues.setAntiAlias(true);
        textPaintSideValues.setColor(DEFAULT_TEXT_COLOR);
        textPaintSideValues.setTextSize(textSize);
        textPaintSideValues.setTextAlign(Paint.Align.LEFT);
        textPaintTopValues = new Paint();
        textPaintTopValues.setAntiAlias(true);
        textPaintTopValues.setColor(DEFAULT_TEXT_COLOR);
        textPaintTopValues.setTextAlign(Paint.Align.LEFT);
        percentList = new ArrayList<>();
    }

    /**
     *
     * @param list The ArrayList of Integer with the range of [0-max].
     */
    public void setDataList(ArrayList<ChartColumnItem> list, double max){
        mItems = list;
        targetPercentList = new ArrayList<>();
        if(max == 0) max = 1;
        myMax = max;

        for(ChartColumnItem element : list){
            targetPercentList.add(1-element.getRealValue()/max);
        }

        // Make sure percentList.size() == targetPercentList.size()
        if(percentList.isEmpty() || percentList.size() < targetPercentList.size()){
            int temp = targetPercentList.size() - percentList.size();
            for(int i = 0; i < temp; i++){
                percentList.add(1d);
            }
        } else if (percentList.size()>targetPercentList.size()){
            int temp = percentList.size()-targetPercentList.size();
            for(int i = 0; i < temp; i++){
                percentList.remove(percentList.size()-1);
            }
        }
        setMinimumWidth(2);
        removeCallbacks(animator);
        post(animator);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawGraphicBar(canvas);
    }

    private void drawGraphicBar(Canvas canvas){

        if(percentList != null && !percentList.isEmpty()){
            int lastPosition = 0;
            int newYPosition;
            ChartColumnItem column;

            for(int i = 0; i < percentList.size(); i++){
                column = mItems.get(i);
                newYPosition = lastPosition + (int) column.getColumnWidth();
                drawPrincipalBar(canvas, i, lastPosition, newYPosition, column);
                drawGraphicBarSideValues(canvas, column, newYPosition);
                drawGraphicBarTopValues(canvas, i, lastPosition);
                //drawBottomGraphicLines(canvas, lastPosition, newYPosition, column);
                lastPosition += getBarMargin(column) + (int) column.getColumnWidth() + mTextSideHeight;
            }
        }
    }

    private void drawPrincipalBar(Canvas canvas, int position, int lastPosition, int newYPosition,
                                  ChartColumnItem chartColumnItem){
        rect.set(0,
                lastPosition,
                (getWidth() - topMargin - (int) ((getWidth() - topMargin) * percentList.get(position)) + BAR_DEFAULT_AMOUNT),
                newYPosition);
        fgPaint.setColor(chartColumnItem.getColumnBackgroundColor());
        canvas.drawRect(rect, fgPaint);
    }

    private int getBarMargin(ChartColumnItem chartColumnItem){
        return BAR_SIDE_MARGIN + (int) chartColumnItem.getBottomTextSize();
    }

    private void drawGraphicBarSideValues(Canvas canvas, ChartColumnItem item, int lastPosition) {
            textPaintSideValues.setTypeface(item.getColumnBottomTextTypeface());
            textPaintSideValues.setTextSize(item.getBottomTextSize());
            textPaintSideValues.setColor(item.getBottomTextColor());
            canvas.drawText(item.getBottomText(),
                    mSideBarLabelXAxisPosition,
                    getBottomTextYPosition(item.getBottomTextSize(), lastPosition),
                    textPaintSideValues);
    }

    private float getBottomTextYPosition(float textsize, int lastPosition){
        return mTextSideHeight + textsize + lastPosition;
    }

    private void drawGraphicBarTopValues(Canvas canvas, int incrementalCounter, int lastPosition) {

            textPaintTopValues.setTypeface(mItems.get(incrementalCounter).getColumnCenterTextTypeface());
            textPaintTopValues.setTextSize(mItems.get(incrementalCounter).getCenterTextSize());
            textPaintTopValues.setColor(mItems.get(incrementalCounter).getCenterTextColor());

            if(targetPercentList.get(incrementalCounter) > 0.70 && !isTimeValues){

                canvas.drawText(mItems.get(incrementalCounter).getCenterText(),
                        getCenterTextOutXPosition(incrementalCounter),
                        getCenterTextOutYPosition(incrementalCounter, lastPosition, textPaintTopValues),
                        textPaintTopValues);

            } else if (targetPercentList.get(incrementalCounter) > 0.60 && isTimeValues) {

                canvas.drawText(mItems.get(incrementalCounter).getCenterText(),
                        getCenterTextOutXPosition(incrementalCounter),
                        getCenterTextOutYPosition(incrementalCounter, lastPosition, textPaintTopValues),
                        textPaintTopValues);

            } else {//Show values inside graphic bar

                canvas.drawText(mItems.get(incrementalCounter).getCenterText(),
                        getCenterTextInXPosition(incrementalCounter),
                        getCenterTextInYPosition(incrementalCounter, lastPosition, textPaintTopValues),
                        textPaintTopValues);
            }
    }

    private float getCenterTextOutYPosition(int incrementalCounter, int lastPosition, Paint paint){
        return lastPosition + (mItems.get(incrementalCounter).getColumnWidth() / 2) - ((paint.descent() + paint.ascent()) / 2);
    }

    private float getCenterTextInYPosition(int incrementalCounter, int lastPosition, Paint paint){
        return lastPosition + (mItems.get(incrementalCounter).getColumnWidth() / 2) - ((paint.descent() + paint.ascent()) / 2);
    }

    private float getCenterTextOutXPosition(int incrementalCounter){
        return (getWidth() - topMargin - (int) ((getWidth() - topMargin) * percentList.get(incrementalCounter))) +
                getResources().getDimension(R.dimen.last_week_horizontal_graphic_top_numeric_value_factor);
    }

    private float getCenterTextInXPosition(int incrementalCounter){
        return (getWidth() - topMargin - (int) ((getWidth() - topMargin) * percentList.get(incrementalCounter)))
                - getValueToShowLabelInsideGraphicBar(mItems.get(incrementalCounter).getCenterText());
    }

    private int getValueToShowLabelInsideGraphicBar(String valueLabel){
        int value = 0;

        for(char currentChar : valueLabel.toCharArray()){
            if(Character.isDigit(currentChar))
                value +=  getResources().getDimension(R.dimen.last_week_horizontal_graphic_top_numeric_value_factor);
            else
                value +=  getResources().getDimension(R.dimen.last_week_horizontal_graphic_top_non_numeric_value_factor);
        }

        return value;
    }

    private void drawBottomGraphicLines(Canvas canvas, int startPositionY, int endPositionY, ChartColumnItem chartColumnItem){
                fgPaint.setColor(chartColumnItem.getColumnBackgroundColor());
                rect.set(0, startPositionY, 2, endPositionY);
                canvas.drawRect(rect, fgPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int mViewWidth = measureWidth(widthMeasureSpec);
        int mViewHeight = measureHeight(heightMeasureSpec);
        setMeasuredDimension(mViewWidth,mViewHeight);
    }

    private int measureWidth(int measureSpec){
        int preferred = 100;

        return getMeasurement(measureSpec, preferred);
    }

    private int measureHeight(int measureSpec){
        int preferred = 0;

        for(ChartColumnItem column: mItems){
            preferred += getBarMargin(column) + (int) column.getColumnWidth() + mTextSideHeight;
        }

        return getMeasurement(measureSpec, preferred);
    }

    private int getMeasurement(int measureSpec, int preferred) {
        int specSize = MeasureSpec.getSize(measureSpec);
        int measurement;

        switch (MeasureSpec.getMode(measureSpec)) {
            case MeasureSpec.EXACTLY:
                measurement = specSize;
                break;
            case MeasureSpec.AT_MOST:
                measurement = Math.min(preferred, specSize);
                break;
            default:
                measurement = preferred;
                break;
        }

        return measurement;
    }

    public void isTimeValues(boolean timeValues) {
        isTimeValues = timeValues;
    }
}