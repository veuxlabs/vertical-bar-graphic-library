package com.verticalbargraphiclibrary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by daniel on 09/10/15.
 */
public class VerticalBarGraphicView extends View {

    private final int BAR_DEFAULT_AMOUNT = 30;
    private static final int DEFAULT_BACKGROUND_COLOR = Color.parseColor("#f2f3f2");
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#ffffff");
    private static final int DEFAULT_FOREGROUND_COLOR = Color.parseColor("#c41230");
    private static final int DEFAULT_BOTTOM_LINE_COLOR= Color.parseColor("#6d6d6d");
    private static final int DEFAULT_GRAPHIC_BAR_WIDTH = 38;
    private static final int DEFAULT_GRAPHIC_BAR_SIDE_MARGIN = 5;
    private static final int DEFAULT_GRAPHIC_BAR_BOTTOM_MARGIN = 8;
    private static final int DEFAULT_LEFT_SIDE_TEXT_BOTTOM_MARGIN = 10;
    private static final int DEFAULT_BACKGROUND_LINES_BOTTOM_MARGIN = 6;
    private static final int DEFAULT_GRAPHIC_TOP_MARGIN = 35;
    private static final int DEFAULT_GRAPHIC_LEFT_MARGIN = 35;
    private static final int DEFAULT_BOTTOM_TEXT_SIZE = 13;
    private static final int DEFAULT_LEFT_TEXT_SIZE = 13;
    private ArrayList<Double> mPercentList;
    private ArrayList<Double> mTargetPercentList;
    private ArrayList<String> mTopTextList = new ArrayList<String>();
    private ArrayList<String> mBottomTextList = new ArrayList<String>();
    private Paint mBottomTextPainter;
    private Paint mTopValuesTextPainter;
    private Paint mLeftSideTextPainter;
    private Paint mBackgroundLinesPainter;
    private Paint mBackgroundPaint;
    private Paint mGraphicBarsPainter;
    private Paint mBottomLinePainter;
    private Rect mRect;
    private Context mContext;
    private int mGraphicBarWidth;
    private double mMaximumGraphicValue = 0;
    private int mBottomTextDescent;
    private boolean mAutoSetWidth = true;
    private int mGraphicTopMargin;
    private int mBottomTextHeight;
    private int mSpaceBetweenGraphicBars;
    private int mGraphicBarsBottomMargin;
    private int mBackgroundLinesBottomMargin;
    private int mLeftSideTextBottomMargin;
    private int mGraphicComponentLeftMargin;
    private boolean mDrawTopValues;
    private boolean mDrawLeftSideValues;
    private boolean mDrawBottomValues;
    private boolean mIsMaxValueZero;
    private Boolean mIsTime = false;
    private Typeface mGraphicLeftSideTextTypeface;
    private Typeface mGraphicBottomTextTypeface;
    private Typeface mGraphicBarsTopValuesTypeface;
    private boolean printDifferentColorFirstColumn;
    private boolean mDrawBottomLine;
    private boolean mCalculateAutomaticBarsWidth;
    private int mFirstColumnColor;
    private int mOthersColumnsColor;

    public VerticalBarGraphicView(Context context){
        this(context,null);
    }

    public VerticalBarGraphicView(Context context, AttributeSet attrs){
        super(context, attrs);
        mContext = context;
        initMiscellaneousValues();
        initMeasureDefaultValues();
        initTextPainters();
    }

    private void initMiscellaneousValues(){
        initGraphicsBarPainter();
        mPercentList = new ArrayList<>();
        mDrawTopValues = false;
        mDrawBottomValues = false;
        mDrawLeftSideValues = false;
        mRect = new Rect();
    }

    private void initMeasureDefaultValues(){
        mGraphicComponentLeftMargin = GraphicUtils.dip2px(mContext, DEFAULT_GRAPHIC_LEFT_MARGIN);
        mGraphicTopMargin = GraphicUtils.dip2px(mContext, DEFAULT_GRAPHIC_TOP_MARGIN);
        mGraphicBarWidth = GraphicUtils.dip2px(mContext, DEFAULT_GRAPHIC_BAR_WIDTH);
        mSpaceBetweenGraphicBars = GraphicUtils.dip2px(mContext, DEFAULT_GRAPHIC_BAR_SIDE_MARGIN);
        mGraphicBarsBottomMargin = GraphicUtils.dip2px(mContext, DEFAULT_GRAPHIC_BAR_BOTTOM_MARGIN);
        mLeftSideTextBottomMargin = GraphicUtils.dip2px(mContext, DEFAULT_LEFT_SIDE_TEXT_BOTTOM_MARGIN);
        mBackgroundLinesBottomMargin = GraphicUtils.dip2px(mContext, DEFAULT_BACKGROUND_LINES_BOTTOM_MARGIN);
    }

    private void initGraphicsBarPainter(){
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(DEFAULT_BACKGROUND_COLOR);
        mBottomLinePainter = new Paint(mBackgroundPaint);
        mBottomLinePainter.setColor(DEFAULT_BOTTOM_LINE_COLOR);
        mGraphicBarsPainter = new Paint(mBackgroundPaint);
        mGraphicBarsPainter.setColor(DEFAULT_FOREGROUND_COLOR);
    }

    private void initTextPainters(){
        initBottomTextPainter();
        initTopValuesTextPainter();
        initLeftSideTextPainter();
        initBackgroundLinesPainter();
    }

    private void initBottomTextPainter(){
        int textSize = GraphicUtils.sp2px(mContext, DEFAULT_BOTTOM_TEXT_SIZE);
        mBottomTextPainter = new Paint();
        mBottomTextPainter.setAntiAlias(true);
        mBottomTextPainter.setTextSize(textSize);
        mBottomTextPainter.setTextAlign(Paint.Align.CENTER);
        mBottomTextPainter.setColor(DEFAULT_TEXT_COLOR);
    }

    private void initTopValuesTextPainter(){
        int textSize = GraphicUtils.sp2px(mContext, DEFAULT_BOTTOM_TEXT_SIZE);
        mTopValuesTextPainter = new Paint();
        mTopValuesTextPainter.setAntiAlias(true);
        mTopValuesTextPainter.setTextSize(textSize);
        mTopValuesTextPainter.setTextAlign(Paint.Align.CENTER);
        mTopValuesTextPainter.setColor(DEFAULT_TEXT_COLOR);
    }

    private void initLeftSideTextPainter(){
        int textSize = GraphicUtils.sp2px(mContext, DEFAULT_LEFT_TEXT_SIZE);
        mLeftSideTextPainter = new Paint();
        mLeftSideTextPainter.setAntiAlias(true);
        mLeftSideTextPainter.setTextSize(textSize);
        mLeftSideTextPainter.setTextAlign(Paint.Align.CENTER);
        mLeftSideTextPainter.setColor(DEFAULT_TEXT_COLOR);
    }

    private void initBackgroundLinesPainter(){
        mBackgroundLinesPainter = new Paint();
        mBackgroundLinesPainter.setAntiAlias(true);
        mBackgroundLinesPainter.setTextAlign(Paint.Align.CENTER);
        mBackgroundLinesPainter.setColor(DEFAULT_TEXT_COLOR);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param graphicBarWidth
     */
    public void setGraphicBarWidth(float graphicBarWidth) {
        mGraphicBarWidth = GraphicUtils.dip2px(mContext, graphicBarWidth);;
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param sideMargin
     */
    public void setGraphicBarSideMargin(float sideMargin){
        mSpaceBetweenGraphicBars = GraphicUtils.dip2px(mContext, sideMargin);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param bottomMargin
     */
    public void setGraphicBarBottomMargin(float bottomMargin){
        mGraphicBarsBottomMargin = GraphicUtils.dip2px(mContext, bottomMargin);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param bottomMargin
     */
    public void setLeftSideTextBottomMargin(float bottomMargin){
        mLeftSideTextBottomMargin = GraphicUtils.dip2px(mContext, bottomMargin);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param bottomMargin
     */
    public void setBackgroundLinesBottomMargin(float bottomMargin){
        mBackgroundLinesBottomMargin = GraphicUtils.dip2px(mContext, DEFAULT_BACKGROUND_LINES_BOTTOM_MARGIN);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param topMargin
     */
    public void setGraphicTopMargin(float topMargin){
        mGraphicTopMargin = GraphicUtils.dip2px(mContext, topMargin);
    }
    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param leftMargin
     */
    public void setGraphicLeftMargin(float leftMargin){
        mGraphicComponentLeftMargin = GraphicUtils.dip2px(mContext, leftMargin);
    }
    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param textSize
     */
    public void setGraphicBottomTextSize(float textSize){
        int bottomTextSize = GraphicUtils.sp2px(mContext, textSize);
        mBottomTextPainter.setTextSize(bottomTextSize);
    }

    /**
     * Should receive the value in DP. Following best practices the value should be extracted
     * from a dimens.xml file and be in DP units.
     * @param textSize
     */
    public void setGraphicLeftSideTextSize(float textSize){
        int leftTextSize = GraphicUtils.sp2px(mContext, textSize);
        mLeftSideTextPainter.setTextSize(leftTextSize);
    }

    public void setDrawTopValues(boolean drawTopValues) {
        mDrawTopValues = drawTopValues;
    }

    public void setDrawBottomValues(boolean drawBottomValues) {
        mDrawBottomValues = drawBottomValues;
    }

    public void setDrawLeftSideValues(boolean drawLeftSideValues){
        mDrawLeftSideValues = drawLeftSideValues;
    }

    public void setGraphicBarsColor(int color){
        mGraphicBarsPainter.setColor(color);
    }

    public void setBottomTextColor(int color) {
        mBottomTextPainter.setColor(color);
    }

    public void setLeftSideTextColor(int color) {
        mLeftSideTextPainter.setColor(color);
    }

    public void setBackgroundLinesColor(int color) {
        mBackgroundLinesPainter.setColor(color);
    }

    public void setIsTimeData(Boolean isTime) {
        this.mIsTime = isTime;
    }

    public void setGraphicLeftSideTextTypeface(Typeface typeface){
        mGraphicLeftSideTextTypeface = typeface;
        mLeftSideTextPainter.setTypeface(mGraphicLeftSideTextTypeface);
    }

    public void setGraphicBarsBottomTextTypeface(Typeface typeface){
        mGraphicBottomTextTypeface = typeface;
        mBottomTextPainter.setTypeface(mGraphicBottomTextTypeface);
    }

    public void setGraphicBarsTopValuesTypeface(Typeface typeface){
        mGraphicBarsTopValuesTypeface = typeface;
        mTopValuesTextPainter.setTypeface(mGraphicBarsTopValuesTypeface);
    }

    /**
     * Sets the text list for values on top of graphic bars.
     * @param topStringList The String ArrayList in the top.
     */
    public void setTopTextList(ArrayList<String> topStringList){
        this.mTopTextList = topStringList;
    }

    public void setBottomTextList(ArrayList<String> bottomStringList){
        this.mBottomTextList = bottomStringList;
        Rect rect = new Rect();
        mBottomTextDescent = 0;

        for(String textValue : mBottomTextList){
            mBottomTextPainter.getTextBounds(textValue,0,textValue.length(),rect);
            if(mBottomTextHeight < rect.height()){
                mBottomTextHeight = rect.height();
            }
            if(mAutoSetWidth &&(mGraphicBarWidth < rect.width())){
                mGraphicBarWidth = rect.width();
            }
            if(mBottomTextDescent < (Math.abs(rect.bottom))){
                mBottomTextDescent = Math.abs(rect.bottom);
            }
        }
        setMinimumWidth(2);
        postInvalidate();
    }

    /**
     *
     * @param valuesList The ArrayList of Doubles with the range of [0-max].
     */
    public void setDataList(ArrayList<Double> valuesList, double maxListValue){
        mTargetPercentList = new ArrayList<>();
        setMaximumGraphicValue(valuesList, maxListValue);

        // Make sure mPercentList.size() == mTargetPercentList.size()
        if(mPercentList.isEmpty() || mPercentList.size() < mTargetPercentList.size()){
            int temp = mTargetPercentList.size() - mPercentList.size();
            for(int i = 0; i < temp; i++){
                mPercentList.add(1d);
            }
        } else if (mPercentList.size() > mTargetPercentList.size()){
            int temp = mPercentList.size() - mTargetPercentList.size();
            for(int i = 0; i < temp; i++){
                mPercentList.remove(mPercentList.size() - 1);
            }
        }

        setMinimumWidth(2);
        removeCallbacks(animator);
        post(animator);
    }

    private Runnable animator = new Runnable() {
        @Override
        public void run() {
            boolean needNewFrame = false;
            for (int counter = 0; counter < mTargetPercentList.size(); counter++) {
                if (mPercentList.get(counter) < mTargetPercentList.get(counter)) {
                    mPercentList.set(counter, mPercentList.get(counter) + 0.02f);
                    needNewFrame = true;
                } else if (mPercentList.get(counter) > mTargetPercentList.get(counter)){
                    mPercentList.set(counter, mPercentList.get(counter) - 0.02f);
                    needNewFrame = true;
                }
                if(Math.abs(mTargetPercentList.get(counter) - mPercentList.get(counter)) < 0.02f){
                    mPercentList.set(counter, mTargetPercentList.get(counter));
                }
            }
            if (needNewFrame) {
                postDelayed(this, 20);
            }
            invalidate();
        }
    };

    private void setMaximumGraphicValue(ArrayList<Double> valuesList, double maxListValue){
        if(maxListValue == 0) {
            maxListValue = 1;
            mIsMaxValueZero = true;
        } else
            mIsMaxValueZero = false;

        mMaximumGraphicValue = maxListValue;

        for(Double element : valuesList){
            element = checkIfElementPercentageIsTooLow(element);
            mTargetPercentList.add(1 - element / maxListValue);
        }
    }

    /**
     * If the percentage of a certain element in relation with the max value
     * is huge, this meaning on the element being equal or less than 3% of the max value
     * then we assign the element a four percent value. This is done to be able to establish
     * a difference between this very low values and 0 values and reflect this on the graphic bars.
     * If we don't do this, then this low values will be represented as a 0 value in the bars.
     * @param element
     * @return 4% of the max value if the element value is less than 4% of the max value.
     *         element value in other case.
     */
    private Double checkIfElementPercentageIsTooLow(Double element){
        Double pointFivePercentOfMaxValue = mMaximumGraphicValue * 0.5f / 100f;
        Double fourPercentMaxValue = mMaximumGraphicValue * 4.0f / 100f;

        if(element > pointFivePercentOfMaxValue && element <= fourPercentMaxValue)
            element = fourPercentMaxValue;
        else if(element < pointFivePercentOfMaxValue)
            element = 0d;

        return element;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        if (mCalculateAutomaticBarsWidth)
            loadBarsWidthDependingChartWidth(mPercentList.size());
        drawBackgroundValuesLines(canvas);
        drawLeftSideValues(canvas);
        drawGraphicBar(canvas);
        drawBottomGraphicValues(canvas);
    }

    private void drawLeftSideValues(Canvas canvas){
        if (mIsTime)
            drawTimeValues(canvas);
        else
            drawRegularNumericValues(canvas);
    }

    private void drawTimeValues(Canvas canvas){
        if(mDrawLeftSideValues){
            canvas.drawText(Util.convertSecondsToDuration((long) mMaximumGraphicValue, false), (float) mGraphicComponentLeftMargin / 2, (float) mGraphicTopMargin - mLeftSideTextBottomMargin, mLeftSideTextPainter);
            canvas.drawText(Util.convertSecondsToDuration((long) mMaximumGraphicValue / 2, false), (float) mGraphicComponentLeftMargin / 2, ((getHeight()) / 2), mLeftSideTextPainter);
            canvas.drawText(Util.convertSecondsToDuration(0, false), (float) mGraphicComponentLeftMargin / 2, getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin - mLeftSideTextBottomMargin, mLeftSideTextPainter);
        }
    }

    private void drawRegularNumericValues(Canvas canvas){
        if(mDrawLeftSideValues){
            canvas.drawText(getLeftSideValue(mMaximumGraphicValue), (float) mGraphicComponentLeftMargin / 2, (float) mGraphicTopMargin - mLeftSideTextBottomMargin, mLeftSideTextPainter);
            canvas.drawText(getLeftSideValue(mMaximumGraphicValue / 2), (float) mGraphicComponentLeftMargin / 2, (getHeight()) / 2, mLeftSideTextPainter);
            canvas.drawText(Constants.ZERO, (float) mGraphicComponentLeftMargin / 2, getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin - mLeftSideTextBottomMargin, mLeftSideTextPainter);
        }
    }

    private void drawBackgroundValuesLines(Canvas canvas){
        if(mDrawLeftSideValues){
            canvas.drawLine(0, getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin, (float) getWidth() , getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin, mBackgroundLinesPainter);
            canvas.drawLine(0, (getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin + mGraphicTopMargin) / 2, (float) getWidth() , (getHeight() - mBottomTextHeight - mBackgroundLinesBottomMargin + mGraphicTopMargin) / 2, mBackgroundLinesPainter);
            canvas.drawLine(0, mGraphicTopMargin, (float) getWidth(), mGraphicTopMargin, mBackgroundLinesPainter);
        }
    }

    private String getLeftSideValue(double value){
        if(mIsMaxValueZero == true)
            value = 0;

        if(value > 99){
            return String.valueOf((int)value);//avoid using decimals with values bigger than 99. Could generate space problems on small devices.
        } else {
            return Util.convertDoubleToOneDecimal(value);
        }
    }

    private void drawGraphicBar(final Canvas canvas){
        int incrementalCounter = 1;
        int graphicValuesCounter = 0;
        double percent;
        if(mPercentList != null && !mPercentList.isEmpty()){
            for(Double value : mPercentList) {
                percent = mTargetPercentList.get(incrementalCounter - 1);
                if(mDrawTopValues)
                   drawGraphicTopValues(canvas, incrementalCounter, graphicValuesCounter, percent);
                if (mDrawBottomLine)
                    drawBottomLine(canvas, incrementalCounter);
                if (percent == 1.0)
                    drawBottomLines(canvas, incrementalCounter);
                else
                    drawVerticalBars(canvas, incrementalCounter);
                graphicValuesCounter++;
                incrementalCounter++;
            }
        }
    }

    private void drawVerticalBars(final Canvas canvas, int incrementalCounter){
        /**
         * The correct total height is "getHeight()-mGraphicTopMargin-mBottomTextHeight-mTextTopMarginn",not "getHeight()-mGraphicTopMargin".
         * fix by zhenghuiy@gmail.com on 11/11/13.
         */
        if (printDifferentColorFirstColumn && incrementalCounter == 1){
            mGraphicBarsPainter.setColor(mFirstColumnColor);
        } else if (printDifferentColorFirstColumn && incrementalCounter > 1){
            mGraphicBarsPainter.setColor(mOthersColumnsColor);
        }

        mRect.set(mGraphicComponentLeftMargin + (mSpaceBetweenGraphicBars * incrementalCounter) + (mGraphicBarWidth * (incrementalCounter - 1)),
                mGraphicTopMargin + (int) ((getHeight() - mGraphicTopMargin - mBottomTextHeight) * mPercentList.get(incrementalCounter - 1)),
                mGraphicComponentLeftMargin + ((mSpaceBetweenGraphicBars + mGraphicBarWidth) * incrementalCounter),
                getHeight() - mBottomTextHeight - mGraphicBarsBottomMargin);

        canvas.drawRect(mRect, mGraphicBarsPainter);

    }

    private void drawGraphicTopValues(final Canvas canvas, final int incrementalCounter, final int graphicValuesCounter, double percent) {
        int defaultMargin = 30;

        if (percent == 1.0)
            defaultMargin = 60;

        canvas.drawText(mTopTextList.get(graphicValuesCounter),
                mGraphicComponentLeftMargin + (mSpaceBetweenGraphicBars * incrementalCounter + mGraphicBarWidth * (incrementalCounter - 1) + mGraphicBarWidth / 2),
                mGraphicTopMargin + (int) ((getHeight() - mGraphicTopMargin - mBottomTextHeight) * mPercentList.get(incrementalCounter - 1)) - defaultMargin,
                mTopValuesTextPainter);
    }

    private void drawBottomLines(final Canvas canvas, int incrementalCounter) {
        if (printDifferentColorFirstColumn && incrementalCounter == 1){
            mGraphicBarsPainter.setColor(mFirstColumnColor);
        } else if (printDifferentColorFirstColumn && incrementalCounter > 1){
            mGraphicBarsPainter.setColor(mOthersColumnsColor);
        }

        mRect.set(mGraphicComponentLeftMargin + (mSpaceBetweenGraphicBars * incrementalCounter) + (mGraphicBarWidth * (incrementalCounter - 1)),
                mGraphicTopMargin + (int) ((getHeight() - mGraphicTopMargin - mBottomTextHeight) - (mGraphicBarsBottomMargin + 4)),
                mGraphicComponentLeftMargin + ((mSpaceBetweenGraphicBars + mGraphicBarWidth) * incrementalCounter),
                getHeight() - mBottomTextHeight - mGraphicBarsBottomMargin);
        canvas.drawRect(mRect, mGraphicBarsPainter);
    }

    private void drawBottomLine(final Canvas canvas, int incrementalCounter){
        int marginLeft = 0;

        if (incrementalCounter == 1)
            marginLeft = mGraphicComponentLeftMargin + (mSpaceBetweenGraphicBars * incrementalCounter);


        mRect.set(marginLeft + (mGraphicBarWidth * (incrementalCounter - 1)),
                mGraphicTopMargin + (int) ((getHeight() - mGraphicTopMargin - mBottomTextHeight) - (mGraphicBarsBottomMargin + 2)) + 3,
                mGraphicComponentLeftMargin + ((mSpaceBetweenGraphicBars + mGraphicBarWidth) * incrementalCounter),
                getHeight() - mBottomTextHeight - mGraphicBarsBottomMargin + GraphicUtils.dpToPx(mContext, 1) + 3);

        canvas.drawRect(mRect, mBottomLinePainter);
    }

    private void drawBottomGraphicValues(Canvas canvas) {
        if(mDrawBottomValues){
            if (mBottomTextList != null && !mBottomTextList.isEmpty()) {
                int incrementalCounter = 1;
                for (String textValue : mBottomTextList) {
                    drawBottomValues(canvas, textValue, incrementalCounter);
                    incrementalCounter++;
                }
            }
        }
    }

    private void drawBottomValues(final Canvas canvas, final String textValue, final int incrementalCounter) {
        canvas.drawText(textValue,
                mGraphicComponentLeftMargin + (mSpaceBetweenGraphicBars * incrementalCounter + mGraphicBarWidth * (incrementalCounter - 1) + mGraphicBarWidth / 2),
                getHeight() - mBottomTextDescent,
                mBottomTextPainter);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int mViewWidth = measureWidth(widthMeasureSpec);
        int mViewHeight = measureHeight(heightMeasureSpec);
        setMeasuredDimension(mViewWidth, mViewHeight);
    }

    private int measureWidth(int measureSpec) {
        int preferred = 0;
        if (mTopTextList != null) {
            preferred = (mTopTextList.size() * (mGraphicBarWidth + mSpaceBetweenGraphicBars)) + mGraphicComponentLeftMargin + 1;
        }
        return getMeasurement(measureSpec, preferred);
    }

    private int measureHeight(int measureSpec) {
        int preferred = 222;
        return getMeasurement(measureSpec, preferred);
    }

    private int getMeasurement(int measureSpec, int preferred) {
        int specSize = MeasureSpec.getSize(measureSpec);
        int measurement;
        switch (MeasureSpec.getMode(measureSpec)) {
            case MeasureSpec.EXACTLY:
                measurement = specSize;
                break;
            case MeasureSpec.AT_MOST:
                measurement = Math.min(preferred, specSize);
                break;
            default:
                measurement = preferred;
                break;
        }
        return measurement;
    }

    public void setPrintDifferentColorFirstColumn(boolean printDifferentColorFirstColumn) {
        this.printDifferentColorFirstColumn = printDifferentColorFirstColumn;
    }

    public void setOthersColumnsColor(int mOthersColumnsColor) {
        this.mOthersColumnsColor = mOthersColumnsColor;
    }

    public void setFirstColumnColor(int mFirstColumnColor) {
        this.mFirstColumnColor = mFirstColumnColor;
    }

    public void drawBottomLine(boolean mDrawBottomLine) {
        this.mDrawBottomLine = mDrawBottomLine;
    }

    public void setCalculateAutomaticBarsWidth(boolean mCalculateAutomaticBarsWidth) {
        this.mCalculateAutomaticBarsWidth = mCalculateAutomaticBarsWidth;
    }

    public void loadBarsWidthDependingChartWidth(int barsNum){
        if (barsNum > 0 && !mDrawLeftSideValues){
            mGraphicBarWidth = (getWidth() / barsNum) - (mGraphicComponentLeftMargin + mSpaceBetweenGraphicBars);
        } else if (barsNum > 0 && mDrawLeftSideValues){
            mGraphicBarWidth = (getWidth() / barsNum) - ((mGraphicComponentLeftMargin / barsNum) + mSpaceBetweenGraphicBars);
        }
    }
}
