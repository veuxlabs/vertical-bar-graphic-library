package com.verticalbargraphiclibrary;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.DecimalFormat;

/**
 * Created by danielsanchez on 7/18/16.
 */
public class Util {
    //---------- TIME ---------

    /**
     * This method receives a number of seconds and converts those seconds to a time format that could include seconds
     * depending if the showSeconds parameter is TRUE or FALSE.
     * @param seconds
     * @param showSeconds
     * @return HH:MM:SS if showSeconds was set to TRUE, HH:MM if showSeconds was set to FALSE.
     */
    public static String convertSecondsToDuration(long seconds, boolean showSeconds){
        Duration duration = new Duration(seconds * Constants.MILLISECONDS_FACTOR);
        Period period = duration.toPeriod(PeriodType.yearDayTime());
        StringBuilder stringBuilder = new StringBuilder();

        if(String.valueOf(period.getHours()).length() == 1)
            stringBuilder.append(Constants.ZERO);

        stringBuilder.append(period.getHours()).append(":");

        if(String.valueOf(period.getMinutes()).length() == 1)
            stringBuilder.append(Constants.ZERO);

        if(showSeconds){
            stringBuilder.append(period.getMinutes()).append(":");

            if(String.valueOf(period.getSeconds()).length() == 1)
                stringBuilder.append(Constants.ZERO);

            stringBuilder.append(period.getSeconds());
        } else
            stringBuilder.append(period.getMinutes());

        return stringBuilder.toString();
    }

    //---------- DECIMALS ---------

    public static String convertDoubleToTwoDecimals(double value){
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return decimalFormat.format(value);
    }

    public static String convertDoubleToOneDecimal(double value){
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        return decimalFormat.format(value);
    }
}
